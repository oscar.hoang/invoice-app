import { combineReducers } from 'redux';
import invoices from './invoices';
import invoice from './invoice';

export default combineReducers({
  invoices,
  invoice
});
