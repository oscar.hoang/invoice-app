import data from './data.json';

const INITIAL_STATE = {
  invoices: [...data],
  filter: []
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case 'FETCH_INVOICES':
      return state;
    case 'SET_FILTER_INVOICES':
      return {
        ...state,
        filter: action.payload
      };
    case 'RESET_FILTER_INVOICES':
      return {
        ...state,
        filter: action.payload.filter
      };
    case 'UPDATE_INVOICES':
      return {
        ...state,
        invoices: action.payload
      };
    default:
      return state;
  }
};
