const INITIAL_STATE = {};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case 'SET_INVOICE':
      return {
        ...state,
        invoice: action.payload
      };
    default:
      return state;
  }
};
