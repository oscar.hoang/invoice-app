import {
  FETCH_INVOICE,
  FETCH_INVOICES,
  NEW_INVOICE,
  EDIT_INVOICE,
  PAID_INVOICE,
  REMOVE_INVOICE,
  FETCH_FILTER_INVOICES
} from './types.js';

export const getInvoiceAction = (id) => ({
  type: FETCH_INVOICE,
  payload: id
});

export const getInvoicesAction = () => ({
  type: FETCH_INVOICES
});

export const editInvoiceAction = (formData) => ({
  type: EDIT_INVOICE,
  payload: formData
});

export const newInvoiceAction = (formData, status) => ({
  type: NEW_INVOICE,
  payload: {
    invoice: formData,
    status
  }
});

export const markInvoicePaidAction = (id) => ({
  type: PAID_INVOICE,
  payload: id
});

export const removeInvoiceAction = (id) => ({
  type: REMOVE_INVOICE,
  payload: id
});

export const filterInvoicesAction = (status) => ({
  type: FETCH_FILTER_INVOICES,
  payload: status
});
