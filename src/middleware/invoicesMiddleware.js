import {
  FETCH_INVOICE,
  SET_INVOICE,
  NEW_INVOICE,
  EDIT_INVOICE,
  UPDATE_INVOICES,
  PAID_INVOICE,
  REMOVE_INVOICE,
  FETCH_FILTER_INVOICES,
  SET_FILTER_INVOICES,
  RESET_FILTER_INVOICES
} from '../actions/types';

import {
  generateId,
  parseToInt,
  parsePaymentDate,
  parseTotal,
  parseLineItems,
} from '../utils';

const fetchInvoice = (store, invoiceId) => {
  const { invoices } = store.getState();
  const [invoice] = invoices.invoices.filter((inv) => inv.id === invoiceId);
  return {
    type: SET_INVOICE,
    payload: invoice
  };
};

const createInvoice = ({ invoice, status }) => {
  const invoiceDate = new Date(invoice.createdAt);
  const paymentTerms = parseToInt(invoice.paymentTerms);
  const paymentDue = parsePaymentDate(invoiceDate, paymentTerms);
  const total = parseTotal(invoice.items);
  const items = parseLineItems(invoice.items);

  return {
    ...invoice,
    id: generateId(),
    ...(invoice.paymentTerms && { paymentTerms }),
    ...((invoice.createdAt && invoice.paymentTerms) && { paymentDue }),
    status,
    items,
    total
  };
};

const addInvoice = (store, { invoice, status }) => {
  const newInvoice = createInvoice({ invoice, status });

  const { invoices } = store.getState();
  const newArray = [...invoices.invoices, newInvoice];

  return {
    type: UPDATE_INVOICES,
    payload: newArray
  };
};

const editInvoice = (store, invoice) => {
  const { invoices } = store.getState();
  const newArray = [...invoices.invoices];
  const foundIndex = newArray?.findIndex((x) => x.id === invoice.id);
  newArray[foundIndex] = invoice;
  return {
    type: UPDATE_INVOICES,
    payload: newArray
  };
};

const paidInvoice = (store, id) => {
  const { invoices } = store.getState();
  const newArray = [...invoices.invoices];
  const foundIndex = newArray?.findIndex((x) => x.id === id);
  newArray[foundIndex].status = 'paid';
  return {
    type: UPDATE_INVOICES,
    payload: newArray
  };
};

const removeInvoice = (store, id) => {
  const { invoices } = store.getState();
  const filteredArray = invoices.invoices.filter((x) => x.id !== id);
  return {
    type: UPDATE_INVOICES,
    payload: filteredArray
  };
};

const filterInvoices = (store, filterBy) => {
  const { invoices } = store.getState();

  if (filterBy === 'default') {
    return {
      type: RESET_FILTER_INVOICES,
      payload: {
        filter: []
      }
    };
  }
  const filteredArray = invoices.invoices.filter((x) => x.status === filterBy);
  return {
    type: SET_FILTER_INVOICES,
    payload: filteredArray
  };
};

export default (store) => (next) => async (action) => {
  let update;
  switch (action.type) {
    case (FETCH_INVOICE):
      next(action);
      update = await fetchInvoice(store, action.payload);
      store.dispatch(update);
      break;
    case (NEW_INVOICE):
      next(action);
      update = await addInvoice(store, action.payload);
      store.dispatch(update);
      break;
    case (EDIT_INVOICE):
      next(action);
      update = await editInvoice(store, action.payload);
      store.dispatch(update);
      break;
    case (PAID_INVOICE):
      next(action);
      update = await paidInvoice(store, action.payload);
      store.dispatch(update);
      break;
    case (REMOVE_INVOICE):
      next(action);
      update = await removeInvoice(store, action.payload);
      store.dispatch(update);
      break;
    case (FETCH_FILTER_INVOICES):
      next(action);
      update = await filterInvoices(store, action.payload);
      store.dispatch(update);
      break;
    default:
      next(action);
      break;
  }
};
