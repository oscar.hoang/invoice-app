export const generateId = () => {
  let letters = '';
  const characters = 'abcdefghijklmnopqrstuvwxyz';
  for (let i = 0; i < 2; i += 1) {
    letters += characters.charAt(Math.floor(Math.random() * characters.length)).toUpperCase();
  }

  const numbers = Math.floor(1000 + Math.random() * 9000).toString();
  const invoiceId = letters + numbers;
  return invoiceId;
};

export const parseToInt = (num) => parseInt(num, 10);

export const parsePaymentDate = (invoiceDate, paymentTerms) => {
  const dueDate = invoiceDate.setDate(invoiceDate.getDate() + paymentTerms);
  return new Date(dueDate).toISOString().split('T')[0];
};

export const parseTotal = (items) => {
  if (!items.length) return 0;
  return items.reduce((prev, acc) => prev + acc.total, 0);
};

export const parseLineItems = (items) => {
  return items.map((item) => ({
    ...item,
    price: parseToInt(item.price),
    quantity: parseToInt(item.quantity)
  }));
};

export const formatDate = (d) => {
  const date = new Date(d);
  const year = date.getFullYear();
  const monthIndex = date.getMonth() + 1;
  const months = ['January', 'February', 'March', 'April', 'May', 'June',
    'July', 'August', 'September', 'October', 'November', 'December'];
  const day = date.getUTCDate();
  return `${day} ${months[monthIndex].substring(0, 3)} ${year}`;
};
