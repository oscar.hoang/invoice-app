import { ItemProps, AddressProps } from '../redux/store';

interface FormProps {
  createdAt: string;
  description: string;
  paymentTerms: string;
  clientName: string;
  clientEmail: string;
  senderAddress: AddressProps;
  clientAddress: AddressProps;
  items: ItemProps[];
}

const validateFormObj = (obj: {}) => {
  const values:{[index: string]:any} = { ...obj };

  Object.keys(values).forEach((key) => {
    if (values[key] === '') {
      values[key] = false;
    } else {
      values[key] = true;
    }
  });
  const arr = Object.values(values);
  const isValid = arr.every((item) => item === true);
  return isValid;
};

const validateFormArr = (items: any) => {
  const validatedItems = items.map((item: ItemProps) => {
    return validateFormObj(item);
  });
  const isValid = validatedItems.every((item: boolean) => item === true);
  return isValid;
};

export const validateForm = (values: FormProps) => {
  const itemsIsValid = validateFormArr(values.items);
  const clientAddressIsValid = validateFormObj(values.clientAddress);
  const senderAddressIsValid = validateFormObj(values.senderAddress);
  const formIsValid = validateFormObj(values);
  const isValid = itemsIsValid && clientAddressIsValid && senderAddressIsValid && formIsValid;
  return isValid;
};
