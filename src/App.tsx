import React from 'react';
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom';
import { Invoices } from './components/invoices';
import { Invoice } from './components/invoice';
import { CreateInvoiceWrapper } from './components/invoice/CreateInvoiceWrapper';

function App() {
  return (
    <Router>
      <div className='container mx-auto w-11/12 md:w-6/12'>
        <Switch>
          <Route exact path='/' component={Invoices} />
          <Route exact path='/create' component={CreateInvoiceWrapper} />
          <Route exact path='/invoice/:id' render={({ match }) => {
            return <Invoice invoiceId={match?.params?.id} />;
          }} />
          <Route exact path='/invoice/edit/:id' render={({ match }) => {
            return <CreateInvoiceWrapper invoiceId={match?.params?.id} />;
          }} />
          <Route component={() => <div>Page Not Found</div>} />
        </Switch>
      </div>
    </Router>
  );
}

export default App;
