import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import invoices from '../middleware/invoicesMiddleware';
import rootReducer from '../reducers';

export interface ItemProps {
  name: string;
  quantity: number;
  price: number;
  total: number;
  [key:number]: number;
}

export interface AddressProps {
  street: string;
  city: string;
  postCode: string;
  country: string;
}

export interface InvoiceProps {
  id: string;
  createdAt: string;
  paymentDue: string;
  description: string;
  paymentTerms: string;
  clientName: string;
  clientEmail: string;
  status: string;
  senderAddress: AddressProps;
  clientAddress: AddressProps;
  items: [ItemProps];
  total: number
}

export interface RootState {
  invoice: {
    invoice: InvoiceProps;
  };
  invoices: {
    invoices: [InvoiceProps];
    filter: [InvoiceProps];
  };
}

const middleware = [invoices];
const enhancer = composeWithDevTools(
  applyMiddleware(...middleware)
);

const persistConfig = {
  key: 'root',
  storage
};

const persistedReducer = persistReducer(persistConfig, rootReducer);
export const store = createStore(persistedReducer, {}, enhancer);
export const persistor = persistStore(store);
