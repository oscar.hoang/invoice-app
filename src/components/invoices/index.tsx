import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { getInvoicesAction } from '../../actions/invoiceActions';
import { InvoiceItem } from './InvoiceItem';
import { InvoiceProps, RootState } from '../../redux/store';
import { Header } from '../Header';

export const Invoices: React.FC = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    (async () => {
      await dispatch(getInvoicesAction());
    })();
  }, [dispatch]);

  const { invoices, filter } = useSelector((state: RootState) => state.invoices);
  const data = filter?.length ? filter : invoices;

  return (
    <React.Fragment>
      <Header items={invoices.length} />
      <div>
        {data?.map((invoice: InvoiceProps) => (
          <InvoiceItem key={invoice.id} invoice={invoice} />
        ))}
      </div>
    </React.Fragment>
  );
};
