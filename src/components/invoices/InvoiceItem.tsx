import React from 'react';
import { Link } from 'react-router-dom';
import { InvoiceProps } from '../../redux/store';
import { formatDate } from '../../utils/index';
import { StatusLabel } from '../StatusLabel';

export const InvoiceItem: React.FC<{ invoice:InvoiceProps }> = ({ invoice }) => {
  return (
    <Link
      to={`/invoice/${invoice.id}`}
      className='
        grid grid-cols-1 md:grid-cols-6
        gap-4 items-center my-4 p-4 md:p-8
        border-2 rounded-xl shadow-md
      '
    >
      <div className='font-bold'>#{invoice.id}</div>
      <div>
        Due {formatDate(invoice.paymentDue)}
      </div>
      <div className='flex w-100 whitespace-nowrap'>{invoice.clientName}</div>
      <div className='flex items-start md:items-end justify-start md:justify-end font-bold'>{invoice.total}</div>
      <StatusLabel status={invoice.status} />
      <div className='flex items-end justify-end text-4xl'>
        <p className='text-indigo-600'>
          &rsaquo;
        </p>
      </div>
    </Link>
  );
};
