import React from 'react';

export const StatusLabel: React.FC<{ status:string }> = ({ status }) => {
  const renderBg = (type: string) => {
    switch (type) {
      case 'paid':
        return 'green';
      case 'pending':
        return 'red';
      default:
        return 'gray';
    }
  };
  return (
    <div
      className={`
        flex items-center justify-center
        py-3 px-4 rounded-md
        bg-gray-200
        bg-${renderBg(status)}-200 font-bold
      `}
    >
      {status}
    </div>
  );
};
