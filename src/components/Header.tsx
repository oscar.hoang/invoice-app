import React, { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import { filterInvoicesAction } from '../actions/invoiceActions';

interface HeaderProps {
  items: number
}

export const Header: React.FC<HeaderProps> = ({ items }) => {
  const [filterBy, setFilterBy] = useState<string>('');
  const dispatch = useDispatch();
  useEffect(() => {
    if (filterBy) {
      dispatch(filterInvoicesAction(filterBy));
    }
  }, [filterBy]);

  return (
    <header className='container mx-auto my-10'>
      <div className='
        flex flex-col md:flex-row gap-4 md:gap-0
        items-start md:items-center justify-between'
      >
        <div>
          <p className='text-4xl font-extrabold'>Invoices</p>
          <p className='text-sm font-medium'>There are {items > 0 ? `${items} total` : 'No'} Invoices</p>
        </div>
        <div className='flex gap-4'>
          <select
            name='filterBy'
            value={filterBy}
            onChange={(e) => setFilterBy(e.target.value)}
            className='py-2 px-4 rounded-full border-2  text-grey-darkest font-bold'
          >
            <option value='default'>Filter by Status</option>
            <option value='draft'>Draft</option>
            <option value='pending'>Pending</option>
            <option value='paid'>Paid</option>
          </select>
          <Link
            to='/create'
            className='
              flex items-center justify-center
              px-4 md:px-6 md:py-4 bg-purple-500
              rounded-full text-white font-bold text-center
            '
          >
            New Invoice
          </Link>

        </div>
      </div>
    </header>
  );
};
