import React, { useState, useEffect } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { getInvoiceAction, markInvoicePaidAction, removeInvoiceAction } from '../../actions/invoiceActions';
import { ItemProps, RootState } from '../../redux/store';
import { StatusLabel } from '../StatusLabel';

export const Invoice: React.FC<{ invoiceId:string }> = ({ invoiceId }) => {
  const [forceRender, setForceRender] = useState<string>('');
  const [redirectTo, setRedirectTo] = useState<string>('');
  const dispatch = useDispatch();

  useEffect(() => {
    if (invoiceId) {
      dispatch(getInvoiceAction(invoiceId));
    }
  }, [invoiceId]);

  const handleRemoveInvoice = (id:string) => {
    if (!window.confirm(`Are you sure you want to delete #${invoiceId}?`)) { // eslint-disable-line no-alert
      return false;
    }
    dispatch(removeInvoiceAction(id));
    setRedirectTo('/');
    return null;
  };

  const handleMarkInvoicePaid = (id:string) => {
    dispatch(markInvoicePaidAction(id));
    setForceRender((prevState: string) => 're-render');
  };

  const renderPaidAction = (status: string, id: string) => {
    if (status === 'pending') {
      return (
        <button
          onClick={() => handleMarkInvoicePaid(id)}
          className='px-8 py-4 bg-purple-400 rounded-full text-white'
        >
          Mark as Paid
        </button>
      );
    }
    return <div className='px-8 py-4 bg-green-400 rounded-full text-white'>Paid</div>;
  };

  const invoice = useSelector((state: RootState) => state.invoice.invoice);

  if (redirectTo) return <Redirect to={redirectTo} />;

  if (!invoice) return null;
  return (
    <div key={forceRender} className='flex flex-col flex-nowrap'>
      <Link to='/' className='flex items-center gap-4 mt-20'>
        <p className='text-indigo-600 text-4xl'>
          &lsaquo;
        </p>
        <p className='font-bold'>
          Go back
        </p>
      </Link>
      <div
        className='
          flex flex-col md:flex-row gap-4
          items-start md:items-center justify-between
          my-4 p-4 md:p-8
          border-2 rounded-xl shadow-md
        '
      >
        <div className='flex gap-4 items-center'>
          <div>Status</div>
          <StatusLabel status={invoice.status} />
        </div>
        <div className='flex items-center justify-between md:justify-end gap-0 md:gap-4 w-full'>
          <Link
            to={`/invoice/edit/${invoice?.id}`}
            className='px-8 py-4 bg-gray-600 rounded-full text-white'
          >
            Edit
          </Link>
          <button
            onClick={() => handleRemoveInvoice(invoice.id)}
            className='px-8 py-4 bg-red-400 rounded-full text-white'
          >
            Delete
          </button>
          {invoice.status !== 'draft' && renderPaidAction(invoice.status, invoice.id)}
        </div>
      </div>
      <div className='my-4 p-4 md:p-8 border-2 rounded-xl shadow-md'>
        <div className='flex items-center justify-between mb-10'>
          <div className='flex flex-col'>
            <p className='text-2xl font-bold'>#{invoice.id}</p>
            <div>{invoice.description}</div>
          </div>
          <div className='text-right'>
            <div>{invoice.senderAddress.street}</div>
            <div>{invoice.senderAddress.city}</div>
            <div>{invoice.senderAddress.postCode}</div>
            <div>{invoice.senderAddress.country}</div>
          </div>
        </div>
        <div className='grid grid-cols-1 md:grid-cols-3 gap-4 items-start mb-4'>
          <div>
            <div className='mb-8'>
              <p className='font-bold'>Invoice Date</p>
              <p className='text-2xl font-bold'>{invoice.createdAt}</p>
            </div>
            <p className='font-bold'>Payment Due</p>
            <p className='text-2xl font-bold'>{invoice.paymentDue}</p>
          </div>
          <div>
            <div className='my-8'>
              <p className='font-bold'>Bill To</p>
              <p className='text-2xl font-bold'>{invoice.clientName}</p>
            </div>
            <div>{invoice.clientAddress.street}</div>
            <div>{invoice.clientAddress.city}</div>
            <div>{invoice.clientAddress.postCode}</div>
            <div>{invoice.clientAddress.country}</div>
          </div>
          <div>
            <p className='font-bold'>Sent To</p>
            <p className='text-2xl font-bold'>{invoice.clientEmail}</p>
          </div>
        </div>
        <div className='grid grid-cols-4 gap-4 items-start p-8 border-t-2 rounded-t-lg shadow-md'>
          <p className='font-bold'>Item Name</p>
          <p className='font-bold'>QTY.</p>
          <p className='font-bold'>Price</p>
          <p className='font-bold text-right'>Total</p>
          {invoice.items.map((item: ItemProps, index: number) => (
            <React.Fragment key={index}>
              <p>{item.name}</p>
              <p>{item.quantity}</p>
              <p>${item.price}</p>
              <p className='text-right'>${item.total}</p>
            </React.Fragment>
          ))}
        </div>
        <div className='flex items-center justify-between p-8 bg-black rounded-b-lg'>
          <p className='text-white font-bold'>Amount Due</p>
          <p className='text-white text-4xl font-bold'>${invoice.total}</p>
        </div>
      </div>
    </div>
  );
};
