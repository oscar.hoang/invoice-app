import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link, Redirect } from 'react-router-dom';
import { RootState, ItemProps } from '../../redux/store';
import { validateForm } from '../../utils/validation';
import {
  editInvoiceAction,
  newInvoiceAction,
  getInvoiceAction,
} from '../../actions/invoiceActions';

const initialFormState = {
  createdAt: '',
  description: '',
  paymentTerms: '',
  clientName: '',
  clientEmail: '',
  senderAddress: {
    street: '',
    city: '',
    postCode: '',
    country: '',
  },
  clientAddress: {
    street: '',
    city: '',
    postCode: '',
    country: '',
  },
  items: [{
    name: '',
    quantity: 0,
    price: 0,
    total: 0
  }],
  total: 0,
};

export const CreateInvoiceWrapper: React.FC<{ invoiceId:string }> = ({ invoiceId }) => {
  const [redirectTo, setRedirectTo] = useState<string>('');
  const [formValues, setFormValues] = useState(initialFormState);
  const [error, setError] = useState<string>('');

  const dispatch = useDispatch();
  const invoice = useSelector((state: RootState) => state.invoice.invoice);

  useEffect(() => {
    dispatch(getInvoiceAction(invoiceId));
  }, [invoiceId]);

  useEffect(() => {
    if (invoiceId && invoice) {
      setFormValues(invoice);
    }
  }, [invoiceId, invoice]);

  const handleInputChange = (e: any) => {
    const name = e.target.name;
    const value = e.target.value;
    setFormValues((prevForm) => ({
      ...prevForm,
      [name]: value
    }));
  };

  const handleSenderAddressChange = (e: any) => {
    const name = e.target.name;
    const value = e.target.value;
    setFormValues((prevState: any) => ({
      ...prevState,
      senderAddress: {
        ...prevState.senderAddress,
        [name]: value,
      },
    }));
  };

  const handleClientAddressChange = (e: any) => {
    const name = e.target.name;
    const value = e.target.value;
    setFormValues((prevState: any) => ({
      ...prevState,
      clientAddress: {
        ...prevState.clientAddress,
        [name]: value,
      },
    }));
  };

  const handleItemsChange = (i: number, e: any) => {
    const items = [...formValues.items];
    const item: ItemProps = items[i];
    item[e.target.name] = e.target.value;
    item.total = item.quantity * item.price;

    setFormValues((prevState: any) => {
      return {
        ...prevState,
        items
      };
    });
  };

  const addFormFields = () => {
    setFormValues({
      ...formValues,
      items: [
        ...formValues.items,
        {
          name: '',
          quantity: 0,
          price: 0,
          total: 0,
        }
      ]
    });
  };

  const removeFormFields = (i: number) => {
    const items = [...formValues.items];
    items.splice(i, 1);
    setFormValues((prevState: any) => {
      return {
        ...prevState,
        items: [...items]
      };
    });
  };

  const handleDraftInvoice = (e: React.SyntheticEvent) => {
    e.preventDefault();
    dispatch(newInvoiceAction(formValues, 'draft'));
    setRedirectTo('/');
  };

  const handleSubmit = (e: React.SyntheticEvent) => {
    e.preventDefault();

    const isValid = validateForm(formValues);

    if (isValid) {
      setError('');
      if (!invoiceId) {
        dispatch(newInvoiceAction(formValues, 'pending'));
      } else {
        dispatch(editInvoiceAction(formValues));
      }
      setRedirectTo('/');
    } else {
      setError('Please Fill in all the fields');
    }
  };

  if (redirectTo) return <Redirect to={redirectTo} />;
  return (
    <div className='container mx-auto mb-20'>
      <div className='mt-20 mb-10'>
        <p className='text-2xl font-bold'>{invoiceId ? `Edit ${invoiceId}` : 'Create'}</p>
      </div>
      {error && <div>{error}</div>}

      <form onSubmit={handleSubmit}>
        <fieldset className='mb-10'>
          <div className='mb-4'>
            <p className='text-purple-600 font-bold'>Bill From</p>
          </div>
          <div className='flex flex-col'>
            <label>Street Address</label>
            <input
              name='street'
              type='input'
              placeholder=''
              value={formValues.senderAddress.street}
              onChange={(e) => handleSenderAddressChange(e)}
              className='border mb-4 py-2 px-3 text-grey-darkest'
            />
          </div>
          <div className='grid grid-cols-3 gap-4'>
            <div className='flex flex-col'>
              <label>Street Address</label>
              <input
                name='city'
                type='input'
                placeholder=''
                value={formValues.senderAddress.city}
                onChange={(e) => handleSenderAddressChange(e)}
                className='border mb-4 py-2 px-3 text-grey-darkest'
              />
            </div>
            <div className='flex flex-col'>
              <label>Postal Code</label>
              <input
                name='postCode'
                type='input'
                placeholder=''
                value={formValues.senderAddress.postCode}
                onChange={(e) => handleSenderAddressChange(e)}
                className='border mb-4 py-2 px-3 text-grey-darkest'
              />
            </div>
            <div className='flex flex-col'>
              <label>Country</label>
              <input
                name='country'
                type='input'
                placeholder=''
                value={formValues.senderAddress.country}
                onChange={(e) => handleSenderAddressChange(e)}
                className='border mb-4 py-2 px-3 text-grey-darkest'
              />
            </div>
          </div>
        </fieldset>
        <fieldset className='mb-10'>
          <div className='mb-4'>
            <p className='text-purple-600 font-bold'>Bill To</p>
          </div>
          <div className='flex flex-col'>
            <div className='flex flex-col'>
              <label>Client&apos;s Name</label>
              <input
                name='clientName'
                type='input'
                placeholder=''
                value={formValues.clientName}
                onChange={(e) => handleInputChange(e)}
                className='border mb-4 py-2 px-3 text-grey-darkest'
              />
            </div>
            <div className='flex flex-col'>
              <label>Client&apos;s Email</label>
              <input
                name='clientEmail'
                type='input'
                placeholder=''
                value={formValues.clientEmail}
                onChange={(e) => handleInputChange(e)}
                className='border mb-4 py-2 px-3 text-grey-darkest'
              />
            </div>
          </div>
          <div className='flex flex-col'>
            <label>Street Address</label>
            <input
              name='street'
              type='input'
              placeholder=''
              value={formValues.clientAddress.street}
              onChange={(e) => handleClientAddressChange(e)}
              className='border mb-4 py-2 px-3 text-grey-darkest'
            />
          </div>
          <div className='grid grid-cols-3 gap-4'>
            <div className='flex flex-col'>
              <label>City</label>
              <input
                name='city'
                type='input'
                placeholder=''
                value={formValues.clientAddress.city}
                onChange={(e) => handleClientAddressChange(e)}
                className='border mb-4 py-2 px-3 text-grey-darkest'
              />
            </div>
            <div className='flex flex-col'>
              <label>Postal Code</label>
              <input
                name='postCode'
                type='input'
                placeholder=''
                value={formValues.clientAddress.postCode}
                onChange={(e) => handleClientAddressChange(e)}
                className='border mb-4 py-2 px-3 text-grey-darkest'
              />
            </div>
            <div className='flex flex-col'>
              <label>Country</label>
              <input
                name='country'
                type='input'
                placeholder=''
                value={formValues.clientAddress.country}
                onChange={(e) => handleClientAddressChange(e)}
                className='border mb-4 py-2 px-3 text-grey-darkest'
              />
            </div>
          </div>
        </fieldset>
        <fieldset className='mb-8'>
          <div className='flex gap-4'>
            <div className='flex flex-col flex-grow'>
              <label>Invoice Date</label>
              <input
                name='createdAt'
                type='date'
                placeholder=''
                value={formValues.createdAt}
                onChange={(e) => handleInputChange(e)}
                className='border mb-4 py-2 px-3 text-grey-darkest'
              />
            </div>
            <div className='flex flex-col flex-grow'>
              <label>Payment Terms</label>
              <select
                name='paymentTerms'
                value={formValues.paymentTerms}
                onChange={(e) => handleInputChange(e)}
                className='border mb-4 py-2 px-3 text-grey-darkest'
              >
                <option />
                <option value={1}>NET 1</option>
                <option value={7}>NET 7</option>
                <option value={14}>NET 14</option>
                <option value={30}>NET 30</option>
              </select>
            </div>
          </div>
        </fieldset>
        <fieldset className='mb-8'>
          <div className='flex flex-col'>
            <label>Product Description</label>
            <input
              name='description'
              type='input'
              placeholder=''
              value={formValues.description}
              onChange={(e) => handleInputChange(e)}
              className='border mb-4 py-2 px-3 text-grey-darkest'
            />
          </div>
        </fieldset>
        <fieldset>
          <div className='mb-4'>
            <p className='text-2xl font-bold'>Item list</p>
          </div>
          {formValues.items.map((item, index) => {
            return (
              <div key={index} className='grid grid-cols-5 gap-4 items-center'>
                <div className='flex flex-col'>
                  <label>Item Name</label>
                  <input
                    name='name'
                    type='input'
                    placeholder=''
                    value={item.name || ''}
                    onChange={(e) => handleItemsChange(index, e)}
                    className='border mb-4 py-2 px-3 text-grey-darkest'
                  />
                </div>
                <div className='flex flex-col'>
                  <label>Qty.</label>
                  <input
                    name='quantity'
                    type='input'
                    placeholder=''
                    value={item.quantity || ''}
                    onChange={(e) => handleItemsChange(index, e)}
                    className='border mb-4 py-2 px-3 text-grey-darkest'
                  />
                </div>
                <div className='flex flex-col'>
                  <label>Price</label>
                  <input
                    name='price'
                    type='input'
                    placeholder=''
                    value={item.price || ''}
                    onChange={(e) => handleItemsChange(index, e)}
                    className='border mb-4 py-2 px-3 text-grey-darkest'
                  />
                </div>
                <div className='flex flex-col'>
                  <label>Total</label>
                  <input
                    disabled={true}
                    name='total'
                    type='input'
                    placeholder=''
                    value={item.total || ''}
                    onChange={(e) => handleItemsChange(index, e)}
                    className='border mb-4 py-2 px-3 text-grey-darkest'
                  />
                </div>
                <div className='flex flex-col items-center justify-center' onClick={() => removeFormFields(index)}>
                  Remove
                </div>
              </div>
            );
          })}
          <div className='mt-4'>
            <button
              className='py-4 px-6 w-full rounded-full border-2'
              type='button'

              onClick={() => addFormFields()}
            >
              + Add New Item
            </button>
          </div>
        </fieldset>
        <div className='flex items-center justify-between mt-8'>
          <Link
            to='/'
            type='button'
            className='px-6 py-4 bg-white border-2 border-gray-600 rounded-full text-gray-600'
          >
            Discard
          </Link>
          <div className='flex gap-4'>
            <button
              type='submit'
              onClick={(e) => handleDraftInvoice(e)}
              className='px-6 py-4 bg-gray-600 rounded-full text-white'
            >
              Save as Draft
            </button>
            <button
              type='submit'
              className='px-6 py-4 bg-purple-400 rounded-full text-white'
            >
              Save & Send
            </button>
          </div>
        </div>
      </form>
    </div>
  );
};
